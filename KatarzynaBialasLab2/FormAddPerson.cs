﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class FormAddPerson : Form
    {
        public FormAddPerson()
        {
            InitializeComponent();
        }

        public TextBox TextBoxName
        {
            get
            {
                return textBoxName;
            }
        }

       public TextBox TextBoxSurname
        {

            get
            {
                return textBoxSurname;
            }
        }

        public TextBox TextBoxAge
        {
            get
            {
                return textBoxAge;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
