﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab2
{
    class Superman: Person
    {
        public int amountOfHands;
        
        public Superman(string newName, string newSurname, int newAge, int amountOfHands) : base(newName, newSurname, newAge)
        {
            this.amountOfHands = amountOfHands;
        }
    }
}
