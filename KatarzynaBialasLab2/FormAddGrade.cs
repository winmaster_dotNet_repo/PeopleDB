﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class FormAddGrade : Form
    {
        public FormAddGrade()
        {
            InitializeComponent();
        }

        public TextBox TextBoxGrade
        {
            get
            {
                return textBoxGrade;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
