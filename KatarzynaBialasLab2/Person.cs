﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab2
{
    [Serializable]
   public class Person
    {
        //to sa property!!!
       public string Name { get; set; }
       public string Surname { get; set; }
       public int Age { get; set; }

       public List<Grade> Grades  { get;  set;} 

        public Person()
        {

        }

        public Person(string name, string surname, int age)
        {
            this.Name = name;
            this.Surname = surname;
            this.Age = age;
            Grades = new List<Grade>();
        }

        public void addGrade(float grade)
        {
            Grades.Add(new Grade(grade));
        }
    }
}
