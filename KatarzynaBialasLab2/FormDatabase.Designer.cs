﻿namespace KatarzynaBialasLab2
{
    partial class Lab2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameSurname = new System.Windows.Forms.Label();
            this.dataGridViewListOfPeople = new System.Windows.Forms.DataGridView();
            this.buttonShow = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.dataGridViewGrades = new System.Windows.Forms.DataGridView();
            this.labelPeople = new System.Windows.Forms.Label();
            this.labelMarks = new System.Windows.Forms.Label();
            this.buttonAddGrade = new System.Windows.Forms.Button();
            this.buttonShowGrades = new System.Windows.Forms.Button();
            this.pictureBoxSmile = new System.Windows.Forms.PictureBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonReadFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNameSurname
            // 
            this.labelNameSurname.AutoSize = true;
            this.labelNameSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.labelNameSurname.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelNameSurname.Location = new System.Drawing.Point(283, 9);
            this.labelNameSurname.Name = "labelNameSurname";
            this.labelNameSurname.Size = new System.Drawing.Size(273, 39);
            this.labelNameSurname.TabIndex = 0;
            this.labelNameSurname.Text = "Katarzyna Białas";
            // 
            // dataGridViewListOfPeople
            // 
            this.dataGridViewListOfPeople.BackgroundColor = System.Drawing.Color.Aquamarine;
            this.dataGridViewListOfPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfPeople.Location = new System.Drawing.Point(192, 87);
            this.dataGridViewListOfPeople.Name = "dataGridViewListOfPeople";
            this.dataGridViewListOfPeople.Size = new System.Drawing.Size(347, 379);
            this.dataGridViewListOfPeople.TabIndex = 1;
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(29, 137);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(139, 23);
            this.buttonShow.TabIndex = 2;
            this.buttonShow.Text = "Wyświetl";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(29, 166);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(139, 23);
            this.buttonAdd.TabIndex = 6;
            this.buttonAdd.Text = "Dodaj osobe";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // dataGridViewGrades
            // 
            this.dataGridViewGrades.BackgroundColor = System.Drawing.Color.Bisque;
            this.dataGridViewGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGrades.Location = new System.Drawing.Point(566, 87);
            this.dataGridViewGrades.Name = "dataGridViewGrades";
            this.dataGridViewGrades.Size = new System.Drawing.Size(143, 379);
            this.dataGridViewGrades.TabIndex = 10;
            this.dataGridViewGrades.Visible = false;
            // 
            // labelPeople
            // 
            this.labelPeople.AutoSize = true;
            this.labelPeople.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelPeople.Location = new System.Drawing.Point(192, 58);
            this.labelPeople.Name = "labelPeople";
            this.labelPeople.Size = new System.Drawing.Size(67, 26);
            this.labelPeople.TabIndex = 11;
            this.labelPeople.Text = "Osoby";
            // 
            // labelMarks
            // 
            this.labelMarks.AutoSize = true;
            this.labelMarks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelMarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelMarks.Location = new System.Drawing.Point(566, 58);
            this.labelMarks.Name = "labelMarks";
            this.labelMarks.Size = new System.Drawing.Size(68, 26);
            this.labelMarks.TabIndex = 12;
            this.labelMarks.Text = "Oceny";
            this.labelMarks.Visible = false;
            // 
            // buttonAddGrade
            // 
            this.buttonAddGrade.Location = new System.Drawing.Point(29, 224);
            this.buttonAddGrade.Name = "buttonAddGrade";
            this.buttonAddGrade.Size = new System.Drawing.Size(139, 23);
            this.buttonAddGrade.TabIndex = 13;
            this.buttonAddGrade.Text = "Dodaj ocene";
            this.buttonAddGrade.UseVisualStyleBackColor = true;
            this.buttonAddGrade.Click += new System.EventHandler(this.buttonAddGrade_Click);
            // 
            // buttonShowGrades
            // 
            this.buttonShowGrades.Location = new System.Drawing.Point(29, 253);
            this.buttonShowGrades.Name = "buttonShowGrades";
            this.buttonShowGrades.Size = new System.Drawing.Size(139, 23);
            this.buttonShowGrades.TabIndex = 17;
            this.buttonShowGrades.Text = "Wyświetl oceny";
            this.buttonShowGrades.UseVisualStyleBackColor = true;
            this.buttonShowGrades.Click += new System.EventHandler(this.buttonShowGrades_Click);
            // 
            // pictureBoxSmile
            // 
            this.pictureBoxSmile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxSmile.InitialImage = null;
            this.pictureBoxSmile.Location = new System.Drawing.Point(732, 88);
            this.pictureBoxSmile.Name = "pictureBoxSmile";
            this.pictureBoxSmile.Size = new System.Drawing.Size(86, 101);
            this.pictureBoxSmile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSmile.TabIndex = 16;
            this.pictureBoxSmile.TabStop = false;
            this.pictureBoxSmile.Visible = false;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(29, 195);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(139, 23);
            this.buttonRefresh.TabIndex = 18;
            this.buttonRefresh.Text = "Aktualizuj";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(29, 282);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(139, 23);
            this.buttonSave.TabIndex = 19;
            this.buttonSave.Text = "Zapisz do pliku";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonReadFile
            // 
            this.buttonReadFile.Location = new System.Drawing.Point(29, 311);
            this.buttonReadFile.Name = "buttonReadFile";
            this.buttonReadFile.Size = new System.Drawing.Size(139, 23);
            this.buttonReadFile.TabIndex = 20;
            this.buttonReadFile.Text = "Odczyt z pliku";
            this.buttonReadFile.UseVisualStyleBackColor = true;
            this.buttonReadFile.Click += new System.EventHandler(this.buttonReadFile_Click);
            // 
            // Lab2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::KatarzynaBialasLab2.Properties.Resources.background_2;
            this.ClientSize = new System.Drawing.Size(847, 507);
            this.Controls.Add(this.buttonReadFile);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonShowGrades);
            this.Controls.Add(this.pictureBoxSmile);
            this.Controls.Add(this.buttonAddGrade);
            this.Controls.Add(this.labelMarks);
            this.Controls.Add(this.labelPeople);
            this.Controls.Add(this.dataGridViewGrades);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.dataGridViewListOfPeople);
            this.Controls.Add(this.labelNameSurname);
            this.Name = "Lab2";
            this.Text = "Baza";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNameSurname;
        private System.Windows.Forms.DataGridView dataGridViewListOfPeople;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.DataGridView dataGridViewGrades;
        private System.Windows.Forms.Label labelPeople;
        private System.Windows.Forms.Label labelMarks;
        private System.Windows.Forms.Button buttonAddGrade;
        private System.Windows.Forms.PictureBox pictureBoxSmile;
        private System.Windows.Forms.Button buttonShowGrades;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonReadFile;
    }
}

